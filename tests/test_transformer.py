from transformer import *


def test_shape_transformer_standard():

    # Casing
    assert(ShapeTransformer.transform('Hello world') == 'Xxxxx xxxxx')
    assert(ShapeTransformer.transform('Hello world', lowercase=True) == 'xxxxx xxxxx')
    assert(ShapeTransformer.transform('Hello ADELE', lowercase=True) == 'xxxxx xxxxx')

    # Symbols and punctuation
    assert(ShapeTransformer.transform('These cookies are DELICIOUS! :p') == 'Xxxxx xxxxxxx xxx XXXXXXXXX! :x')
    assert(ShapeTransformer.transform('Did you know that (3 × 4) + 2 = 14 ?') == 'Xxx xxx xxxx xxxx (d × d) + d = dd ?')
    assert(ShapeTransformer.transform('I should\'ve paid him $4.00 more, but I didn\'t!') == 'X xxxxxx\'xx xxxx xxx $d.dd xxxx, xxx X xxxx\'x!')

def test_shape_transformer_emoji():

    # Love
    assert(ShapeTransformer.transform('I <3 emoji!') == 'X <d xxxxx!')
    assert(ShapeTransformer.transform('I \u2764 emoji!') == 'X e xxxxx!')
    assert(ShapeTransformer.transform('I ❤ emoji!') == 'X e xxxxx!')
    assert(ShapeTransformer.transform('I ❤️ emoji!') == 'X e xxxxx!')

    # Flags
    assert(ShapeTransformer.transform('🏳') == 'e')
    assert(ShapeTransformer.transform('🇪🇸') == 'e')
    assert(ShapeTransformer.transform('🏳️‍🌈') == 'e')

    # Human
    assert(ShapeTransformer.transform('👩') == 'e')
    assert(ShapeTransformer.transform('👍🏾 🤘 🤙🏻') == 'e e e')
    assert(ShapeTransformer.transform('👨‍👨‍👧‍👧') == 'e')

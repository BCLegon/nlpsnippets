import pytest
from tokenizer import *


@pytest.fixture(scope='module')
def example_sentences():
    return [
        'Hello World!',
        'In 2017, Apple enabled eGPU support in macOS for the first time.',
        'He tweeted: "THE U.K. IS AMAZING!!!"'
    ]


def test_word_tokenizer(example_sentences):
    example_tokens = [
        ['Hello', 'World'],
        ['In', '2017', 'Apple', 'enabled', 'eGPU', 'support', 'in', 'macOS', 'for', 'the', 'first', 'time'],
        ['He', 'tweeted', 'THE', 'U', 'K', 'IS', 'AMAZING']
    ]

    for i in range(len(example_sentences)):
        assert(WordTokenizer.tokenize(example_sentences[i]) == example_tokens[i])


def test_word_tokenizer_lowercase(example_sentences):
    example_tokens = [
        ['hello', 'world'],
        ['in', '2017', 'apple', 'enabled', 'egpu', 'support', 'in', 'macos', 'for', 'the', 'first', 'time'],
        ['he', 'tweeted', 'the', 'u', 'k', 'is', 'amazing']
    ]

    for i in range(len(example_sentences)):
        assert(WordTokenizer.tokenize(example_sentences[i], lowercase=True) == example_tokens[i])


def test_spacing_tokenizer(example_sentences):
    example_tokens = [
        ['Hello', ' ', 'World', '!'],
        ['In', ' ', '2017', ',', ' ', 'Apple', ' ', 'enabled', ' ', 'eGPU', ' ', 'support', ' ', 'in', ' ', 'macOS', ' ', 'for', ' ', 'the', ' ', 'first', ' ', 'time', '.'],
        ['He', ' ', 'tweeted', ':', ' ', '"', 'THE', ' ', 'U', '.', 'K', '.', ' ', 'IS', ' ', 'AMAZING', '!', '!', '!', '"']
    ]

    for i in range(len(example_sentences)):
        assert(SpacingTokenizer.tokenize(example_sentences[i]) == example_tokens[i])


def test_caps_tokenizer(example_sentences):
    example_tokens = [
        ['+SC+', 'hello', ' ', '+SC+', 'world', '!'],
        ['+SC+', 'in', ' ', '2017', ',', ' ', '+SC+', 'apple', ' ', 'enabled', ' ', 'e', '+UC+', 'gpu', ' ', 'support', ' ', 'in', ' ', 'mac', '+UC+', 'os', ' ', 'for', ' ', 'the', ' ', 'first', ' ', 'time', '.'],
        ['+SC+', 'he', ' ', 'tweeted', ':', ' ', '"', '+UC+', 'the', ' ', '+UC+', 'u', '.', '+UC+', 'k', '.', ' ', '+UC+', 'is', ' ', '+UC+', 'amazing', '!', '!', '!', '"']
    ]

    for i in range(len(example_sentences)):
        assert(CapsTokenizer.tokenize(example_sentences[i]) == example_tokens[i])

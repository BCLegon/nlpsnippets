class ContextWindow():

    def __init__(self, words, margin=2):
        self.index = -1
        self.margin = margin
        self.words = words

    def _is_valid_index(self,index):
        if index >= 0 and index < len(self.words):
            return True
        return False

    def get(self, index):
        window = []
        if self._is_valid_index(index):
            window.append(self.words[index])
        for offset in range(1, self.margin+1):
            if self._is_valid_index(index+offset):
                window.append(self.words[index+offset])
            if self._is_valid_index(index-offset):
                window.insert(0, self.words[index-offset])
        return window

    def __iter__(self):
        return self

    def __next__(self):
        if self.index+1 >= len(self.words):
            self.index = -1
            raise StopIteration
        else:
            self.index += 1
            return self.get(self.index)


class ContextDictionary(ContextWindow):

    def _add_to_dict(self, dictionary, key):
        if key in dictionary:
            dictionary[key] += 1
        else:
            dictionary[key] = 1

    def get(self, index, trivial_included=True):
        dictionary = {}
        if trivial_included and self._is_valid_index(index):
            self._add_to_dict(dictionary, self.words[index])
        for offset in range(1, self.margin+1):
            if self._is_valid_index(index+offset):
                self._add_to_dict(dictionary, self.words[index+offset])
            if self._is_valid_index(index-offset):
                self._add_to_dict(dictionary, self.words[index-offset])
        return dictionary

import re
import regex


class Tokenizer():

    @classmethod
    def _lower_tokens(cls, tokens, lowercase=True):
        if lowercase:
            return [token.lower() for token in tokens]
        else:
            return tokens

    @classmethod
    def tokenize(cls, text):
        raise NotImplementedError()

    @classmethod
    def detokenize(cls, tokens):
        raise NotImplementedError()


class WordTokenizer(Tokenizer):
    reg_ex = re.compile(r'\b[-\w]+\b')

    @classmethod
    def tokenize(cls, text, lowercase=False):
        return cls._lower_tokens(cls.reg_ex.findall(text), lowercase)

    @classmethod
    def detokenize(cls, tokens):
        return ''.join([str(token) for token in tokens])


class SpacingTokenizer(WordTokenizer):
    reg_ex = re.compile(r'\w+|\W')


class CapsTokenizer(WordTokenizer):
    reg_ex = regex.compile(
        r'[\p{Ll}\p{Lm}\p{Lo}\p{M}\p{N}]+'
        r'|\p{Lu}[\p{Ll}\p{Lm}\p{Lo}\p{M}\p{N}]*\p{Ll}[\p{Ll}\p{Lm}\p{Lo}\p{M}\p{N}]*'
        r'|[\p{Lu}\p{Lm}\p{Lo}\p{M}\p{N}]+'
        r'|.'
    )
    initial_cap_modifier = '+SC+'  # Sentence case
    all_caps_modifier = '+UC+'  # Uppercase

    @classmethod
    def tokenize(cls, text, lowercase=False):
        fragments = cls.reg_ex.findall(text)
        tokens = list()
        for i in range(len(fragments)):
            if fragments[i] != fragments[i].lower():
                if fragments[i] == fragments[i].upper():
                    tokens.append(cls.all_caps_modifier)
                else:
                    tokens.append(cls.initial_cap_modifier)
            tokens.append(fragments[i].lower())
        return cls._lower_tokens(tokens, lowercase)

    @classmethod
    def detokenize(cls, tokens):
        casing = 0
        fragments = list()
        for i in range(len(tokens)):
            if tokens[i] == cls.initial_cap_modifier:
                casing = 1
            elif tokens[i] == cls.all_caps_modifier:
                casing = 2
            else:
                if casing == 1:
                    fragments.append(tokens[i].capitalize())
                elif casing == 2:
                    fragments.append(tokens[i].upper())
                else:
                    fragments.append(tokens[i])
                casing = 0
        return SpacingTokenizer.detokenize(fragments)


class SentenceTokenizer(Tokenizer):
    # See also https://stackoverflow.com/questions/25735644/python-regex-for-splitting-text-into-sentences-sentence-tokenizing
    reg_ex = re.compile(r'((?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=[.?])\s)')

    @classmethod
    def tokenize(cls, text, lowercase=False):
        return cls._lower_tokens(cls.reg_ex.split(text), lowercase)

    @classmethod
    def detokenize(cls, tokens):
        return ''.join([str(token) for token in tokens])

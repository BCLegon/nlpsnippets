import regex
import emoji


class ShapeTransformer:
    lower_char_reg_ex = regex.compile(r'[\p{Ll}\p{Lm}\p{Lo}]')
    upper_char_reg_ex = regex.compile(r'\p{Lu}')
    digit_reg_ex = regex.compile(r'\p{N}')

    @classmethod
    def _token_shape(cls, token, replace_emoji=True, replace_symbols=False, replace_punctuation=False,
                     remove_marks=True):
        # See also https://www.regular-expressions.info/unicode.html
        token = regex.sub(r'\p{Z}', ' ', token)
        token = cls.lower_char_reg_ex.sub('x', token)
        token = cls.upper_char_reg_ex.sub('X', token)
        token = cls.digit_reg_ex.sub('d', token)
        if replace_emoji:
            token = emoji.get_emoji_regexp().sub('e', token)
        if replace_symbols:
            token = regex.sub(r'\p{S}', 's', token)
        if replace_punctuation:
            token = regex.sub(r'\p{P}', 'p', token)
        if remove_marks:
            token = regex.sub(r'\p{Mc}', ' ', token)
            token = regex.sub(r'[\p{Mn}\p{Me}]', '', token)
        return token

    @classmethod
    def transform(cls, text, lowercase=False):
        text = cls._token_shape(text)
        if lowercase:
            text = text.lower()
        return text

# NLP snippets

[![pipeline status](https://gitlab.com/BCLegon/nlpsnippets/badges/master/pipeline.svg)](https://gitlab.com/BCLegon/nlpsnippets/commits/master)
[![coverage report](https://gitlab.com/BCLegon/nlpsnippets/badges/master/coverage.svg)](https://gitlab.com/BCLegon/nlpsnippets/commits/master)

Useful snippets for Natural Language Processing.
